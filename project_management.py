import gitlab
import urllib
import time
import yaml


debian_gitlab_domain = 'salsa.debian.org'
default_init_branch_name = 'init_ci'
default_master_branch_name = 'master'
default_path_to_ci_yml = 'debian/gitlab-ci.yml'
default_release = 'unstable'
default_salsa_ci_yml = 'https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/salsa-ci.yml'
default_pipeline_jobs_yml = 'https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/pipeline-jobs.yml'


def get_project_by_url(cli, project_url):
    parser = urllib.parse.urlparse(project_url)

    if not parser.netloc == debian_gitlab_domain:
        raise ValueError("Repository is not Salsa")

    project_name = parser.path.split('/')[-1]
    project_path_with_namespace = parser.path.strip('/')

    results = cli.search('projects', project_name)

    if len(results) == 0:
        raise ValueError("Project does't exist")

    project_id = None
    for project in results:
        if project['path_with_namespace'] == project_path_with_namespace:
            project_id = project['id']

    if project_id is None:
        raise ValueError("Project {} doesn't exist on Salsa")

    return cli.projects.get(project_id)


def create_init_branch(project, init_branch_name, master_branch_name):
    try:
        project.branches.get(init_branch_name)
        raise ValueError("Migration branch already exists")
    except gitlab.GitlabGetError:
        pass

    project.branches.create({'branch': init_branch_name,
                             'ref': master_branch_name})


def fill_yml(release, non_free_deps, options=None):
    '''Return the content to be added in the yaml file

    @param release: requested target RELEASE
    @param non_free_deps: whether or not to add contrib and non-free sources
    @param options: is an optional dictionary with additional user supplied
    variables
    '''

    if options is None:
        options = {}

    ci_yml = {
            'include': [
                default_salsa_ci_yml,
                default_pipeline_jobs_yml
                ]
            }

    if not release == default_release:
        options['RELEASE'] = release

    if non_free_deps:
        options['SALSA_CI_COMPONENTS'] = 'main contrib non-free'

    if options:
        ci_yml['variables'] = options

    return yaml.dump(ci_yml)


def initialize_project(
            cli,
            project,
            init_branch_name=default_init_branch_name,
            master_branch_name=default_master_branch_name,
            ci_config_path=default_path_to_ci_yml,
            release=default_release,
            non_free_deps=False,
            options=None):

    if options is None:
        options = {}

    create_init_branch(project, init_branch_name, master_branch_name)
    project.ci_config_path = ci_config_path
    project.save()

    ci_config_file_content = fill_yml(release, non_free_deps, options)

    commit_data = {
            'branch': init_branch_name,
            'commit_message': 'Salsa CI automatic initialization by Tuco',
            'actions': [
                {
                    'action': 'create',
                    'file_path': ci_config_path,
                    'content': ci_config_file_content
                    }
                ]
            }
    return project.commits.create(commit_data)


def get_pipeline_for(project, triggering_commit):
    pipeline_for_commit = None
    for pipeline in project.pipelines.list():
        if pipeline.sha == triggering_commit.attributes['id']:
            pipeline_for_commit = pipeline
            break
    return pipeline_for_commit


def wait_pipeline_creation_for(project, commit, timeout=60):
    pipeline = None
    while (pipeline is None) and (timeout > 0):
        pipeline = get_pipeline_for(project, commit)
        time.sleep(1)
        timeout -= 1
    return pipeline.id


def wait_pipeline_to_finish(project, pipeline_id, timeout=10*60):
    status = None
    while (status != 'success') and (status != 'failed') and (timeout > 0):
        for pipeline in project.pipelines.list():
            if pipeline.id == pipeline_id:
                status = pipeline.status
                break
        time.sleep(1)
        timeout -= 1
    return status


def merge_initialized_branch(
            project,
            last_commit,
            init_branch_name=default_init_branch_name,
            master_branch_name=default_master_branch_name):

    merge_data = {
            'source_branch': init_branch_name,
            'target_branch': master_branch_name,
            'title': 'Adding Salsa-CI configuration (by Tuco). This MR will be automatically merged if the pipeline succeeds.',
            }
    pipeline = wait_pipeline_creation_for(project, last_commit)

    if pipeline is None:
        raise RuntimeError

    mr = project.mergerequests.create(merge_data)

    # Sometimes it gets merged with no waiting for the pipeline...
    time.sleep(15)
    mr.merge(should_remove_source_branch=True, merge_when_pipeline_succeeds=True)
    return mr
