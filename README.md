Tuco
====

Tuco is a tool aimed to help you initialize your package repository with Salsa
CI basic configurations.

Tuco follows the steps mentioned in the documentation of the pipeline (https://salsa.debian.org/salsa-ci-team/pipeline). It makes four steps:

* Creates working branch
* Configure CI yml path in the project
* Adds Salsa CI yml configuration file to that branch (this it's going to run the pipeline)
* Creates MR

The MR will be automatically merged by gitlab on pipeline success.


Debian installation
===================

Clone the repository and install:

```bash
$ apt install python3-gitlab
$ apt install python3-yaml
```


Virtualenv installation
=======================

Clone the repository and inside the project:

```bash
$ virtualenv -p python3 venv
$ . venv/bin/activate
(venv)$ pip install -r requirements.txt
```


Usage
=====

Running the script with --help will print the command help.

```bash
$ python tuco.py --help
usage: tuco.py [-h] [-m MASTER_BRANCH_NAME] [-i INIT_BRANCH_NAME]
               [-c CI_CONFIG_PATH] [-r RELEASE] [-n] [-o OPTION]
               url

Tuco: Add CI spices to your package. Configure gitlab ci config path and
creates merge request with needed configuration file with automerge if
pipeline success.

positional arguments:
  url                   URL of the project to be initialize.

optional arguments:
  -h, --help            show this help message and exit
  -m MASTER_BRANCH_NAME, --master-branch MASTER_BRANCH_NAME
                        Name of the master branch (default: master).
  -i INIT_BRANCH_NAME, --initialization-branch INIT_BRANCH_NAME
                        Name of the initialization working branch (default:
                        init_ci).
  -c CI_CONFIG_PATH, --ci-config-path CI_CONFIG_PATH
                        CI configuration file path (default: debian/gitlab-
                        ci.yml).
  -r RELEASE, --release RELEASE
                        CI configuration file path (default: unstable).
  -n, --non-free-deps   Active this option if non free dependencies needed.
  -o OPTION, --option OPTION
                        Additional option variables to be set in yaml file.
```

Token generation
----------------

First you need to generate an API token as is explained in the gitlab
documentation and export the environment variable 

```bash
$ export GITLAB_API_TOKEN="xxxxxxxxxxxxxxxxxxxx"
```

Running the script
------------------

The simplest example is running the script only with the project URL:

```bash
$ python3 tuco.py https://salsa.debian.org/group/project
```
